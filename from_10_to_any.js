let a = 24538057437; //Start number
const b = 2; //number system

let div = a; //division result
let rmnd = a % b; //remainder of the division

let result = ""; //I create a result variable and assign it a string type

do {
  //The peculiarity of the cycle is that it is guaranteed to be done once
  result += rmnd; //append to the result and assign a new value to it

  div /= b; //division and assignment

  div = Math.floor(div); //rounding down
  //console.log(div); //prints the result every loop
  rmnd = div % b; //calculates the remainder of the division and assigns this value to a variable
} while (div > 0); //Loop condition

//console.log(result); //output the "result" variable to the console
console.log(result.split("").reverse().join("")); //creates an array and reverses it

// !!! SAVE CODE BEFORE LAUNCH!!!
