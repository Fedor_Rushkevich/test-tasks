// Sum two numbers in reverse code

//functions
function backcode(arrOne) {
  console.log("---Back code function started---");
  if (arrOne[arrOne.length - 1] > 0) {
    arrOne = transportRemainder(arrOne, 1, 1, arrOne.length - 1);
    console.log("Exit 1");
  } else {
    arrOne[arrOne.length - 1] += 1;
    console.log("Exit 2");
  }
  console.log("---Back code fucntion over---");
  return arrOne;
}

function transportRemainder(arr, remainder, num, numForloop) {
  let _arr = [...arr];
  console.log("");
  console.log("--Transport Remainder running--");
  for (let w = numForloop; w >= 0; w--) {
    console.log("try " + w);
    if (_arr[w] != num) {
      _arr[w] += remainder;
      remainder = 0;
      console.log("1 Exit");
    } else {
      _arr[w] = 0;
      console.log("1.2 Exit");
    }
    if (remainder != 1) {
      console.log("2 Exit");
      break;
    }
  }
  console.log("--End of Transport Remainder--");
  console.log("");
  return _arr;
}

function inversion(arr) {
  let _arr = [...arr];
  console.log("--Inversion Running--");
  for (let a = 0; a < _arr.length; a++) {
    if (_arr[a] > 0) {
      _arr[a] = 0;
      console.log(a + " element more then zero");
      console.log("");
    } else {
      _arr[a] = 1;
      console.log(a + " element less then one");
      console.log("");
    }
  }
  console.log("--Inversion Over--");
  console.log("");
  return _arr;
}

function sumOfArrays(arrOne, arrTwo, currentNum) {
  if (currentNum != this.NumberOfBytes) {
    sum = arrOne[currentNum] + arrTwo[currentNum];
  }
  return sum;
}

//-------------------------------------------------------------------------------------------
//code

// let numOne = 48; //First number
// let numTwo = 48; //Second number
function reversecodesum(numOne, numTwo, NumberOfBytes) {
  let arrOne;
  let arrTwo;
  let symbolone = 0; //this will be sign symbol for first array
  let symboltwo = 0; //this will be sign symbol for second array
  let result = [];
  let remainder = 0; //this remainder we will use to go from straight code to reverse
  //const NumberOfBytes = 8;
  let testArr = [];
  let _arrOne;
  let _arrTwo;

  if (numOne < 0) {
    symbolone += 1;
    numOne *= -1;
  }
  if (numTwo < 0) {
    symboltwo += 1;
    numTwo *= -1;
  }

  arrOne = numOne.toString(2).split("");
  arrTwo = numTwo.toString(2).split("");

  for (let r = 0; r < arrOne.length; r++) {
    arrOne[r] = +arrOne[r];
  }

  for (let q = 0; q < arrTwo.length; q++) {
    arrTwo[q] = +arrTwo[q];
  }

  if (arrOne.length != NumberOfBytes) {
    x = arrOne.length;
    for (let m = 0; m < NumberOfBytes - x; m++) {
      arrOne.unshift(0);
    }
    x = 0;
  }

  if (arrTwo.length != NumberOfBytes) {
    x = arrTwo.length;
    for (let n = 0; n < NumberOfBytes - x; n++) {
      arrTwo.unshift(0);
    }
    x = 0;
  }

  _arrOne = [...arrOne];
  _arrTwo = [...arrTwo];

  if (symbolone != 1) {
    console.log("symbol one > 0");
    console.log("");
  } else {
    _arrOne = inversion(arrOne);
  }

  if (symboltwo != 1) {
    console.log("symbol two > 0");
    console.log("");
  } else {
    _arrTwo = inversion(arrTwo);
  }

  if (result.length != NumberOfBytes) {
    x = result.length;
    for (let d = 0; d < NumberOfBytes - x; d++) {
      result.unshift(0);
    }
    x = 0;
  }

  console.log(" First Number in binary " + arrOne);
  console.log(" Second Number in binary " + arrTwo);
  console.log("");
  console.log("***First Step ended***");
  console.log("");

  //In this part we are converting reverse code to back code

  if (symbolone != 0) {
    _arrOne = backcode(_arrOne);
  }
  if (symboltwo != 0) {
    _arrTwo = backcode(_arrTwo);
  }

  console.log("***Second step ended***");
  console.log("");

  //In this part of code we are summning two numbers  and getting result

  console.log("--Sum program started--");
  console.log("");
  console.log("First num " + _arrOne);
  console.log("Second num " + _arrTwo);
  console.log("");

  for (let i = NumberOfBytes - 1; i > -1; i -= 1) {
    if (sumOfArrays(_arrOne, _arrTwo, i) >= 2) {
      remainder = 1;
      result[i] = 0;
      console.log("Current remainer is " + remainder);
      console.log("Exit 1");
    } else {
      result[i] = sumOfArrays(_arrOne, _arrTwo, i);
      console.log("Exit 2");
    }
    if (remainder > 0) {
      testArr = [..._arrOne];
      console.log("Remainder (1)");
      _arrOne = transportRemainder(_arrOne, remainder, 1, i - 1);
      console.log(testArr);
      console.log(_arrOne);
      console.log("Exit 3");
      if (_arrOne != testArr) {
        remainder = 0;
        console.log("Exit 4");
      }
    }

    testArr = testArr.splice(0, testArr.length);

    if (remainder > 0) {
      testArr = [..._arrTwo];
      console.log("Remainder (2)");
      _arrTwo = transportRemainder(_arrTwo, remainder, 1, i - 1);
      if (testArr != _arrTwo) {
        remainder = 0;
      }
    }

    testArr = testArr.splice(0, testArr.length);

    if (remainder > 0) {
      testArr = [...result];
      console.log("Remainder (3)");
      result = transportRemainder(result, remainder, 1, i - 1);
      if (testArr != result) {
        remainder = 0;
      }
    }
  }
  console.log("");
  console.log("***Third step ended***");
  console.log("");

  console.log(result);
  console.log("");

  //symbolone = result.shift();
  result = result.join();
  result = result.replace(/,/g, "");
  result = parseInt(result, 2);

  console.log("___<RESULTS>___");

  //.join()
  console.log("arrOne result " + _arrOne);
  console.log("arrTwo result " + _arrTwo);
  console.log("result " + result);
  console.log("");
  return result;
}

reversecodesum(44, 144, 8);

//-------------------------------------------------------------------

//Garbage

//console.log(resultSum);

// for (let i = 0; i < arrOne.length; i++) arrOne[i] = +arrOne[i] | 0;
//console.log(arrOne);
// for (let t = 0; t < arrOne.length; t++) {
//   if (arrOne[t] > 0) {
//     console.log("Вывожу элемент номер 3 снизу => кол-во раз" + t);
//     console.log(arrOne[3]);
//     arrOne[t] = 0;
// arrOne.forEach(function (i) {
//   if (arrOne[i] == 0) arrOne[i] = 1;
//   else {
//     arrOne[i] = 0;
//   }
// });
//   }
//   if (arrOne[t] < 1) {
//     arrOne[t] = 1;
//   }
// }
//}

// if (symboltwo > 0) {
//   symboltwo = 0;
//   arrTwo = numTwo.toString().split("");
//   for (i = 0; i < arrTwo.length; i++) arrTwo[i] = +arrTwo[i] | 0;
//   console.log(arrTwo);
//   for (t = 0; t < arrOne.length; t++) {
//     if (arrOne[t] != "0") {
//       arrOne[t] = 0;
//     }
//     if (arrOne[t] != "1") {
//       arrOne[t] = 1;
//     }
//   }
//}

// differenceBetweenArrays(arrOne, arrTwo, largest, smallest);

// for (let g = 0; g < difference; g++) {
//   smallest(arrOne, arrTwo).unshift(0);
// }

//arrOne.unshift(symbolone); //unshift add symbol to the front on array
//arrTwo.unshift(symboltwo); //

// function largest(arrOne, arrTwo) {
//   if (arrOne.length > arrTwo.length) {
//     //console.log("1");
//     return arrOne;
//   } else {
//     //console.log("2");
//     return arrTwo;
//   }
// }

// function howMuchIsMissingUntilEight(arr, test) {
//   funcres = test.length - arr.length;
//   return fancres;
// }

// function differenceBetweenArrays(arrOne, arrTwo, largest, smallest) {
//   difference = largest(arrOne, arrTwo).length - smallest(arrOne, arrTwo).length;
//   return difference;
// }

// function smallest(arrOne, arrTwo) {
//   if (arrOne.length < arrTwo.length) {
//     //console.log("-1");
//     return arrOne;
//   } else {
//     //console.log("0");
//     return arrTwo;
//   }
// }

// console.log(symbolone);
// console.log(symboltwo);

// for (let r = 0; r < arrOne.length; r++) {
//   arrOne[r] = +arrOne[r];
//   console.log(arrOne);
// }

// for (let q = 0; q < arrTwo.length; q++) {
//   arrTwo[q] = +arrTwo[q];
// }

// function inversion(_arr) {
//   arr = [..._arr];
//   console.log("before " + arr);
//   let length = arr.length;
//   for (let i = 0; i < length; i++) {
//     if (arr[i] != 0) {
//       arr[i] = 0;
//     } else {
//       arr[i] = 1;
//     }
//   }
//   console.log("after" + arr);
//   _arr.splice(0, _arr.length);
//   _arr.push(arr);
//   console.log(_arr);
//   if (_arr.length > 8) {
//     let diff = _arr.length - 8;
//     for (let c = 0; c < diff; c++) {
//       _arr.pop();
//     }
//   }
//   console.log(_arr);
//   return _arr;
// }

// function inversion(arr) {
//   arr.forEach((element) => {
//     if (element == 0) {
//       element = 1;
//     } else {
//       element = 0;
//     }
//   });
//   return arr;
// }

//x = i - 1;
//   do {
//     if (arrOne[x] < 1) {
//       arrOne[x] += remainder;
//       console.log(arrOne);
//       remainder = 0;
//       console.log("done3");
//     }
//     //   if(symbolone != 0){
//     //   if (x < 0) {
//     //     break;
//     //   }
//     // }
//     else {
//       if (arrTwo[x] < 1) {
//         arrTwo[x] += remainder;
//         console.log(arrTwo);
//         remainder = 0;
//         console.log("done3");
//       }
//     }
//     x -= 1;

//     if (x < 0) {
//       if (symbolone != 1 && symboltwo != 1) console.log("done4");
//       {
//         console.log("done6");
//         break;
//       }
//     } else {
//       {
//         for (let a = NumberOfBytes - 1; a >= 0; a--) {
//           if (result[a] < 3) {
//             result[a] += remainder;
//           }
//         }
//       }
//       console.log("done5");
//     }
//   } while (remainder >= 1);
// }
// function addBinary(a, b) {
//   let sum = "";
//   let carry = "";

//   for (var i = a.length - 1; i >= 0; i--) {
//     if (i == a.length - 1) {
//       //half add the first pair
//       const halfAdd1 = halfAdder(a[i], b[i]);
//       sum = halfAdd1[0] + sum;
//       carry = halfAdd1[1];
//     } else {
//       //full add the rest
//       const fullAdd = fullAdder(a[i], b[i], carry);
//       sum = fullAdd[0] + sum;
//       carry = fullAdd[1];
//     }
//   }

//   return carry ? carry + sum : sum;
// }

// function halfAdder(a, b) {
//   const sum = xor(a, b);
//   const carry = and(a, b);
//   return [sum, carry];
// }

// function fullAdder(a, b, carry) {
//   halfAdd = halfAdder(a, b);
//   const sum = xor(carry, halfAdd[0]);
//   carry = and(carry, halfAdd[0]);
//   carry = or(carry, halfAdd[1]);
//   return [sum, carry];
// }

// //logic gates
// function xor(a, b) {
//   return a === b ? 0 : 1;
// }
// function and(a, b) {
//   return a == 1 && b == 1 ? 1 : 0;
// }
// function or(a, b) {
//   return a || b;
// }

//functions

// function transportRemainder(arr, remainder, num) {
//   for (let w = arr.length - 1; w >= 0; w--) {
//     console.log("try " + w);
//     if (arr[w] != num) {
//       arr[w] += remainder;
//       remainder = 0;
//       console.log("enter" + 1);
//       break;
//     }
//     if (remainder != 1) {
//       console.log("enter" + 2);
//       break;
//     }
//   }
//   console.log("end3");
//   return arr;
// }
