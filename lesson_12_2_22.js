let fruits = ["Orange", "Banana", "Apple"];

// console.log(basket);
// console.log(basket[1]);

let length = fruits.length;
// console.log(length);
// for (let i = 0; i < length; i++) {
//   if (fruits[i] === "Apple") {
//     console.log("True");
//     console.log(i);
//   } else {
//     console.log("False");
//   }
// }

//------------------------------------------------------------------

// if (fruits.includes("Apple")) {
//   console.log("True");
// } else {
//   console.log("False");
// }

//------------------------------------------------------------------

fruits.push("Cherry"); // push добавляет элемент в массив сзади
console.log(fruits);

//------------------------------------------------------------------

// delete fruits[3];      //Первый метод удаление элемента массива ,но остается empty
// console.log(fruits);

//------------------------------------------------------------------

// fruits = fruits.pop(); //Должно удалять последний элемент массива при этом без empty
// console.log(fruits);

// //------------------------------------------------------------------

// fruits = fruits.shift(); //Должно удалять первый элемент массива при этом без empty
// console(fruits);

//------------------------------------------------------------------

//fruits.splice(start number,delete count);  //Удаляем конкретный элемент из массива

const fruit = "Banana"; // Если введенное значение есть в массиве , удалить его из массива и вывести массив ,а если нет вывести -1

if (fruits.includes(fruit)) {
  const index = fruits.indexOf(fruit);

  fruits.splice(index, 1);
} else {
  console.log("-1");
}
console.log(fruits);

//------------------------------------------------------------------

// for (let i = 0; i < fruits.length; i++) {
//   fruits[i] = fruits[i].toUpperCase(); //To upper case  превращает массив в большие буквы
// }
// console.log(fruits);

//------------------------------------------------------------------

// function makeBig(fruit) {
//   return fruit.toUpperCase();
// }

// fruits.map((fruit) => fruit.toUpperCase());
// console.log(fruits);

//------------------------------------------------------------------

// // Фрукт - это строка с названием фрукта: "Orange", "Banana", "Apple"
//
// // 1) Создать корзину fruits с тремя фруктами "Orange", "Banana", "Apple"
// const fruits = ['Orange', 'Banana', 'Apple'];
// console.log(fruits)
//
// // 2) Посмотреть на 2-й фрукт (вывести его имя)
// // console.log(fruits[1]);
//
// // 3) Посмотреть на i-й фрукт (i вводится с клавиатуры)
// // const i = +prompt('Enter number');
// // console.log(fruits[i]);
//
// // 4) Найти в корзинке фрукт "Apple". Вывести на консоль true если есть
// // for (let i = 0; i < fruits.length; i++) {
// //     if ("Cherry" === fruits[i]) {
// //         console.log(true);
// //     }
// // }
// // console.log(fruits.indexOf('Banana'));
//
// // 5) Найти в корзинке фрукт, имя которого вводится с клавиатуры.
// // Если такого нет, то вывести -1
// // const fruit = prompt('Enter fruit name');
// // if (!fruits.includes(fruit)) {
// //     console.log(-1);
// // }
//
// // 6) Добавить в корзинку новый фрукт "Cherry"
// fruits.push('Pineapple');
// // console.log(fruits);
// // 7) Добавить в корзинку фрукт который вводится с клавиатуры
// // const fruit = prompt('Enter fruit name');
// // fruits.push(fruit);
// // console.log(fruits);
//
// // 7) Убрать из корзинки фрукт
// // delete fruits[1];
// // console.log(fruits);
//
// // console.log(fruits.shift());
// // console.log(fruits);
//
// // 8) Забрать по одному фрукту из корзины. И сразу выводить имя фрукта.
// // while (fruits.length > 0) {
// //     const fruit = fruits.pop();
// //     console.log(fruit, fruits);
// // }
//
// // 8) Убрать из корзинки фрукт, имя которого вводится с клавиатуры.
// // Если такого нет, то вывести -1. Иначе имя фрукта, который убрали.
// // const fruit = prompt('enter fruit name');
//
// // if (fruits.includes(fruit)) {
// //     const index = fruits.indexOf(fruit);
//
// //     fruits.splice(index, 1);
// // } else {
// //     console.log(-1);
// // }
//
// // console.log(fruits);
//
// // 9) Длина каждого фрукта - кол-во символов в названии.
// // Вывести длину каждого фрукта. Apple - 5 Orange - 6
// // for (let i = 0; i < fruits.length; i++) {
// //     console.log(`${fruits[i]} - ${fruits[i].length}`);
// // }
//
//
// // 10) Отсортировать фрукты в корзине по длине фрукта
//
// // function compare(str1, str2) {
// //     if (str1.length < str2.length) {
// //         return 1;
// //     } else if (str1.length === str2.length) {
// //         return 0;
// //     } else if (str1.length > str2.length) {
// //         return -1;
// //     }
// // }
//
// // fruits.sort(compare);
// // console.log(fruits);
//
// // 2 способ сортивроки
// // fruits.sort((str1, str2) => str1.length - str2.length);
// // console.log(fruits);
//
// // 11) Фрукт большой если каждый символ в названии - большая буква.
// // Каждый фрукт сделать большим
// // for (let i = 0; i < fruits.length; i++) {
// //     fruits[i] = fruits[i].toUpperCase();
// // }
// // console.log(fruits);
//
// // function makeBig(fruit) {
// //     return fruit.toUpperCase();
// // }
//
// // fruits.map((fruit) => fruit.toUpperCase());
//
// // 12) Каждый фрукт преобразовать
// // в 'Apple' => { name: 'Apple', weight: 0 }
// const newFruits = fruits.map((fruit) => { return { name: fruit, weight: 0 }; });
//
// console.log(fruits, newFruits);
