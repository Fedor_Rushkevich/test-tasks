// let arr = [1, 2, 3, 4, 5];

//for (let i = 0; i < 5; i++) {
//  console.log(arr[i]);
//}
//let length = arr.length;

//let k = 0;

//while (k < length) {
//  console.log(arr[k]);
//  k++;
//}

// for
// while
// from start to end

//-----------------------
//let length = arr.length;

//for (let i = 0; i <= length; i++) {
//console.log(arr[length - i]);
//}

// let length = arr.length;
// let i = 1;
//
// while (i <= length) {
// console.log(arr[length - i]);
// i++;
// }

// let length = arr.length;
//
// for (let i = length - 1; i >= 0; i--) {
// console.log(arr[i]);
// }

//------------------------

// let arr = [1, 2, 3, 4, 5];
// let length = arr.length;
// let result = 0;
//
// for (let i = 0; i < length; i++) {
// result += arr[i];
// }
//
// console.log(result);

// for
// while

//------------------------
function printArray(arr) {
  let length = arr.length;
  let resultOutput = "";

  for (let i = 0; i < length; i++) {
    resultOutput += arr[i] + "  ";
  }

  console.log(resultOutput);
}
//------------------------

// if element of 'arr' is less that 0 -> change the value to 0

// let arr = [1, 2, 3, -4, 5, 0, 190, 25, 12566, 123, -1000, -125, -66, 21134, 0];
// length = arr.length;

// for (let i = 0; i < length; i++) {
//   // if (arr[i] >= 0) {
//   // console.log("do nothing");
//   // } else {
//   // arr[i] = 0;
//   // console.log(arr[i]);
//   // }

//   if (arr[i] < 0) {
//     arr[i] = 0;
//   }
// }

//------------------------

printArray(arr);
