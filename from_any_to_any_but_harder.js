// let numbers = [1, 2, 3, 4];
//
// for (let i = 0; i < numbers.length; i++) {
//   console.log(numbers[i]);
// }
//
// number[2] = 5;
// number[4] = 10;

function converterFromDecimalToCustom(number, base) {
  const alphabet = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
  ];

  console.log(alphabet.length);

  let div = number; // Division

  let lastPosition = 0; // We're gonna use it with rmndArray
  let rmndArray = []; // Array of remainders

  let result = ""; // Function result

  //   do {

  // div /= base;                        // Self division on base
  // div = Math.floor(div);              // Use 'Math.floor' to cut of the number tail

  // rmndArray[lastPosition] = div % base;                  // Get new remainder according to new 'div' value
  // lastPosition += 1;                                      // Increase id, to move forward in 'rmndArray'
  //   } while (div > 0);

  while (div > 0) {
    rmndArray[lastPosition] = div % base;
    lastPosition += 1;

    div /= base;
    div = Math.floor(div);
  }

  for (let i = 1; i <= lastPosition; i++) {
    let currentPosition = lastPosition - i;

    let currentRmndArrayElement = rmndArray[currentPosition];

    result += alphabet[currentRmndArrayElement];
  }

  return result;
}

console.log(converterFromDecimalToCustom(12341234, 20));
