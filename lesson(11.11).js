// вывести третий символ строки
// console.log(str[2]);

// Во второй строке выведите предпоследний символ этой строки.
// console.log(str[str.length - 2]);

// В третьей строке выведите первые пять символов этой строки.
// 1 способ
let output = "";

for (let i = 0; i < 5; ++i) {
  output = output + str[i];
}

// console.log(output);

// 2 способ
// const strCopy = str.slice(0, 5);
// console.log(strCopy);

// В четвертой строке выведите всю строку, кроме последних двух символов.
// const strCopy = str.slice(0, str.length - 2);
// console.log(strCopy);

// В пятой строке выведите все символы с четными индексами
// (считая, что индексация начинается с 0, поэтому символы выводятся начиная с первого).
// let output = '';

// for (let i = 1; i < str.length; i = i + 1) {
//     if (i % 2 === 0) {
//         output = output + str[i];
//     }
// }

// console.log(output);

// В шестой строке выведите все символы с нечетными индексами,
// то есть начиная со второго символа строки.
// let output = '';

// for (let i = 0; i < str.length; i = i + 1) {
//     if (i % 2 !== 0) {
//         output = output + str[i];
//     }
// }

// console.log(output);

// В седьмой строке выведите все символы в обратном порядке.
// 1 способ
// let output = '';

// for (let i = str.length - 1; i >= 0; i = i - 1) {
//    output = output + str[i];
// }

// console.log(output);

// 2 способ
// let output = str.split('').reverse().join('');
// console.log(output);

// В восьмой строке выведите все символы
// строки через три в обратном порядке, начиная с последнего.
// let output = '';

// for (let i = str.length - 1; i >= 0; i = i - 4) {
//     output = output + str[i];
// }

// console.log(output);

// выведите длинну строки
// console.log(str[str.length - 1], ' ', str.length);
